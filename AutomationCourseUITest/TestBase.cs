﻿using System;
using System.Drawing.Imaging;
using System.IO;
using AutomationCourseFramework.WebDriver;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Support.Extensions;

namespace AutomationCourseUITest
{
    public abstract class TestBase
    {
        [TearDown]
        protected void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                try
                {
                    var screenshotDir = $"{AppDomain.CurrentDomain.BaseDirectory}\\screenshots";
                    if (!Directory.Exists(screenshotDir))
                    {
                        Directory.CreateDirectory(screenshotDir);
                    }

                    var screenshot = DriverController.GetInstance().TakeScreenshot();
                    screenshot.SaveAsFile($@"{screenshotDir}\{TestContext.CurrentContext.Test.Name}{DateTime.Now.Ticks}.png", ImageFormat.Png);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TAKE SCREENSHOT ERROR " + ex);
                }
            }
        }
    }
}
