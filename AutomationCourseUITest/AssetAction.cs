﻿using System;
using System.Diagnostics;
using System.Threading;
using AutomationCourseFramework.Pages;
using AutomationCourseFramework.Pages.Archives;
using AutomationCourseFramework.Pages.Archives.Tab;
using AutomationCourseFramework.Pages.Assets.AssetInformation;
using AutomationCourseFramework.WebDriver;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomationCourseUITest
{
    [TestFixture]
    public class AssetAction : TestBase
    {
        private IWebDriver _driver;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _driver = DriverController.GetInstance();

            LoginPage loginPage = new LoginPage();
            loginPage.LoginOrEmail.ClearAndKeyPress("vitaliy.dmitriev@fastdev.se");
            loginPage.Password.ClearAndKeyPress("1QQQqqq");
            loginPage.Login.Click();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            _driver.Quit();
        }

        [TearDown]
        public void TearDown()
        {
            new BasePage().RestoreParentFrame();

            _driver.Navigate().GoToUrl("http://autotestcourse.fastdev.se");
            LogoutFromFotoweb();            
        }

        #region Auxiliary testing methods
        void LogoutFromFotoweb()
        {
            _driver.FindElement(By.XPath("//li[@id='userMenuButton']/a")).Click();
            WaitForUserMenulIsVisible();
            _driver.FindElement(By.XPath("//li[@id='userMenuButton']//div[@class='userMenuOption logout']")).Click();
        }

        public void WaitForActionPanelIsVisible()
        {
            WaitForCondition(() => _driver.FindElement(By.Id("grid-action-panel-wrapper")).
                GetAttribute("Class").Contains("grid-action-panel-open"),
                "Timeout of waiting for visibility of Action panel expired.");
            Thread.Sleep(500);
        }

        public void WaitForUserMenulIsVisible()
        {
            WaitForCondition(() => _driver.FindElement(By.Id("userMenuButton")).
                GetAttribute("Class").Contains("header-size-element open"),
                "Timeout of waiting for UserMenu expired.");
            Thread.Sleep(500);
        }

        public void WaitForStatusModalDialogIsVisible()
        {
            WaitForCondition(() => _driver.FindElement(By.XPath("//div[@class='modal in']")).
                GetAttribute("aria-hidden").Equals("false"),
                "Timeout of waiting for Status model dialog expired.");
            Thread.Sleep(500);
        }

        public static void WaitForCondition(Func<bool> condition, string message)
        {
            WaitForCondition(condition, message, TimeSpan.FromSeconds(60), TimeSpan.FromMilliseconds(500));
        }

        public static void WaitForCondition(Func<bool> condition, string message, TimeSpan waitingTime, TimeSpan interval)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!condition())
            {
                if (stopwatch.Elapsed.TotalSeconds > waitingTime.TotalSeconds)
                    throw new Exception(message);
                Thread.Sleep(interval);
            }
        }
        #endregion

        [Test]
        public void CheckRating()
        {
            const int rating = 3;

            var archivesPage = new ArchivesPage();
            archivesPage.LoadDirectly();
            var commonAssetsArchive = archivesPage.GetArchive("Common assets");
            commonAssetsArchive.Open();

            //Open Action panel for selected asset
            var archivesGridPage = new ArchiveGridPage();
            var archiveAsset = archivesGridPage.GetArchiveAsset("2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg");
            archiveAsset.Select();
            
            var actionPanel = new CridActionPanel();
            actionPanel.SetRating(rating);
            
            var assetDetailsPage = archiveAsset.Open();
            Assert.AreEqual(rating, assetDetailsPage.Rating, "Rating has wrong value");            
        }

        [Test]
        public void CheckStatus()
        {
            var archivesPage = new ArchivesPage();
            archivesPage.LoadDirectly();
            var commonAssetsArchive = archivesPage.GetArchive("Common assets");
            commonAssetsArchive.Open();

            //Open Action panel for selected asset
            var archivesGridPage = new ArchiveGridPage();
            var archiveAsset = archivesGridPage.GetArchiveAsset("2014-05-02T070757Z_1460091317_GM1EA520KIQ01_RTRMADP_3_LEONE-SEACUCUMBERS.jpg");
            archiveAsset.Select();

            var actionPanel = new CridActionPanel();
            actionPanel.SetStatus(AssetStatus.Seven);

            var assetDetailsPage = archiveAsset.Open();
            Assert.AreEqual(AssetStatus.Seven, assetDetailsPage.Status, "Status has wrong value");
        }

    }
}
