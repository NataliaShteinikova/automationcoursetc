﻿using System;
using AutomationCourseFramework.Pages.Assets.AssetInformation;
using AutomationCourseFramework.Pages.PageElements;
using OpenQA.Selenium;

namespace AutomationCourseFramework.Pages.Assets
{
    public class AssetDetailPage : BasePage
    {
        public AssetDetailPage()
        {
            SwitchToContainerFrame();
        }

        public void SwitchToContainerFrame()
        {
            WaitForFraimReadyAndSwitchToIt(By.Id("preview-iframe"));
        }

        public int Rating
        {
            get
            {
                var currentRatingString = new Label(By.XPath("//span[starts-with(@class, 'rating-info')]")).GetAttributeValue("class");

                try
                {
                    var splitedRatingStrs = currentRatingString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var ratingStr = splitedRatingStrs[splitedRatingStrs.Length - 1];

                    return ParceRating(ratingStr);
                }
                catch (Exception ex)
                {

                    throw new Exception("Couldn't get asset rating", ex);
                }
            }

            set
            {
                if (value < 1 && value > 5)
                    throw new Exception($"Couldn't set Ratingg value. Rating value can be witin 1-5. Current value is {value}");

                new Button(By.XPath($"//span[starts-with(@class, 'rating-info')]/i[@data-rating='{value}']")).Click();

                new Label(By.XPath("//div[@id='preview-title']//div[starts-with(@class, 'metadataField inputField')]")).Click(); // I have to lose focus to get a valid value. Maybe it's a parcing trouble. TODO: improve implementation
               WaitForCondition(() => Rating == value, "Changing rating value timeout expired"); // In the main interface star gets the new value after getting "Done" from a background task
            }
        }

        public AssetStatus Status
        {
            get
            {
                var currentStatusString = new Label(By.XPath("//span[contains(@class, 'js-status-bar status-bar')]")).GetAttributeValue("class"); //return starts-with after a developer will trim spaces
                return AssetStatusParcer.Parce(currentStatusString);
            }
        }

        internal static int ParceRating(string ratingString)
        {
            if (ratingString.EndsWith("-"))
                return 0;

            return Convert.ToInt32(ratingString.Split(new[] { "rate-" }, StringSplitOptions.None)[1]);
        }
    }
}
