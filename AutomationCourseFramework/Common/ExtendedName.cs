﻿using System;
using System.Linq;
using System.Reflection;

namespace AutomationCourseFramework.Common
{
    public class ExtendedName : Attribute
    {
        public ExtendedName(string text)
        {
            Text = text;
        }

        public ExtendedName(string text, string additionalInfo)
        {
            Text = text;
            AdditionalInfo = additionalInfo;
        }

        public string Text { get; set; }
        public string AdditionalInfo { get; set; }

        public static T GetEnumByExtendedName<T>(string extendedName) where T : struct, IConvertible
        {
            return GetEnumByName<T>(en => en.GetExtendedName() == extendedName);
        }

        public static T GetEnumByAdditionalInfo<T>(string additionalInfo) where T : struct, IConvertible
        {
            return GetEnumByName<T>(en => en.GetAdditionalInfo() == additionalInfo);
        }

        private static T GetEnumByName<T>(Func<Enum, bool> func) where T : struct, IConvertible
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new Exception("Cannot get enum using ExtendedInfo attribure. T must be an enumerated type");

            var enumValue = Enum.GetValues(type)
                                .Cast<Enum>()
                                .FirstOrDefault(func);

            if (enumValue == null)
                throw new Exception($"Enum with type '{type}' doesn't contain required value");

            return (T)Enum.Parse(type, enumValue.ToString());
        }
    }

    public static class EnumExtedNameExtensions
    {
        public static string GetExtendedName(this Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(ExtendedName), false);

                if (attrs.Length > 0)
                    return ((ExtendedName)attrs[0]).Text;
            }
            return en.ToString();
        }

        public static string GetAdditionalInfo(this Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(ExtendedName), false);

                if (attrs.Length > 0)
                    return ((ExtendedName)attrs[0]).AdditionalInfo;
            }
            return en.ToString();
        }
    }
}
